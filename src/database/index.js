import {initializeApp} from 'firebase'

const app = initializeApp({
  apiKey: "AIzaSyAHz5_RGPfLrUK3YXquu39ObrkPgsOq31I",
  authDomain: "vueapp-777.firebaseapp.com",
  databaseURL: "https://vueapp-777.firebaseio.com",
  projectId: "vueapp-777",
  storageBucket: "vueapp-777.appspot.com",
  // messagingSenderId: "290649799168"
})

export const db = app.database()
export const usersRef = db.ref('users')
export const tasksRef = db.ref('tasks')