import Vue from 'vue'
import Vuex from 'vuex'
import * as firebase from 'firebase'

// import * as storeChunks from '@/store/storeChunks'

Vue.use(Vuex)



const store = {
	state: {
		user: null,
		ui: {
			loading: false,
			error: false
		}
	},
	mutations: {
		setName(state, payload) {
			state.name = payload
		},
		setUser(state, payload) {
			state.user = payload
		},
		setLoading(state, payload) {
			state.ui.loading = payload
		},
		setError(state, payload) {
			state.ui.error = payload
		}
	},
	actions: {
		setName({commit}, payload) {
			commit('setName', payload)
		},
		createUserAction({commit}, payload) {
			const {email, password, component} = payload

			commit('setLoading', true)
			commit('setError', false)

			firebase.auth().createUserWithEmailAndPassword(email, password)
			.then(user => {
				commit('setLoading', false)
				component.$router.push('/signin')
			})
			.catch(error => {
				commit('setLoading', false)
				commit('setError', error)
				console.log('error', error)
			})
		},
		signInUserAction({commit}, payload) {
			const {email, password, component} = payload

			commit('setLoading', true)
			commit('setError', false)
			
			firebase.auth().signInWithEmailAndPassword(email, password)
			.then(user => {
				console.log('user', user.key)
				commit('setUser', {
					id: user.uid,
					email: user.email,
					key: user.key
				})
				// firebase.auth().currentUser.getIdToken().then(token => {
				// 	console.log(token)
				// 	firebase.auth().verifyIdToken(token).then(data => {
				// 		console.log(data)
				// 	})
				// })
				commit('setLoading', false)
				component.$router.push('/profile')
			})
			.catch(error => {
				commit('setLoading', false)
				commit('setError', error)
				console.log('error', error)
			})
		}
	},
	getters: {
		user: state => state.user,
		error: state => state.ui.error,
		loading: state => state.ui.loading,
		test: state => firebase.auth().currentUser
	}
}


export default new Vuex.Store(store)