export const user = {
	state: {
		user: null
	},
	actions: {
		createUserAction({commit}, payload) {
			const {email, password, component} = payload

			commit('setLoading', true)
			commit('setError', false)

			firebase.auth().createUserWithEmailAndPassword(email, password)
			.then(user => {
				commit('setLoading', false)
				component.$router.push('/signin')
			})
			.catch(error => {
				commit('setLoading', false)
				commit('setError', error)
				console.log('error', error)
			})
		},
		signInUserAction({commit}, payload) {
			const {email, password, component} = payload

			commit('setLoading', true)
			commit('setError', false)
			
			firebase.auth().signInWithEmailAndPassword(email, password)
			.then(user => {
				commit('setUser', {
					id: user.uid,
					email: user.email
				})
				// firebase.auth().currentUser.getIdToken().then(token => {
				// 	console.log(token)
				// 	firebase.auth().verifyIdToken(token).then(data => {
				// 		console.log(data)
				// 	})
				// })
				commit('setLoading', false)
				component.$router.push('/')
			})
			.catch(error => {
				commit('setLoading', false)
				commit('setError', error)
				console.log('error', error)
			})
		}
	}
}