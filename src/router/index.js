import VueRouter from 'vue-router'
import * as firebase from 'firebase'

import Pages from '@/pages/'
import store from '@/store/'

const router = new VueRouter({
	mode: 'history',
	routes: [
		// {path: '/', component: Pages.Home, meta: {guard: true}},
		{path: '/', redirect: '/profile'},
		{path: '/profile', component: Pages.Profile, meta: {guard: true}},
		{path: '/todos', component: Pages.Todos, meta: {guard: true}},
		{path: '/signup', component: Pages.Signup},
		{path: '/signin', component: Pages.Signin},
		{path: '*', component: Pages.NotFound}
	]
})

router.beforeEach(async (to, from, next) => {
	if (to.path == '/logout') {
		location.reload(true)
		return next({path: '/'})
		// await firebase.auth().signOut()
		// store.commit('setUser', null)
		// return next({path: '/signin'})
	}

	if (to.meta.guard && !store.state.user) {
		return next({path: '/signin'})
	}

	if (to.path === '/signin' && store.state.user) {
		return next({path: '/profile'})
	}

	next()
})

export default router