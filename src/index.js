import Vue from 'vue'
import Vuex from 'vuex'
import VueFire from 'vuefire'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import * as firebase from 'firebase'
// import admin from 'firebase-admin'
import 'vuetify/dist/vuetify.min.css'


import App from '@/components/'
// import * as GlobalComponents from '@/components/global'
import Alert from '@/components/Alert'
import router from '@/router/'
import store from '@/store/'


// Vue.use(Vuex)
Vue.use(VueFire)
Vue.use(Vuetify)
Vue.use(VueRouter)
Vue.use(VueResource)

Vue.http.options.root = 'http://localhost:3000'

Vue.component('app-alert', Alert)

new Vue({
	el: '#app',
	router,
	store,
	mounted() {
		// if (!this.$store.getters.user) {
		// 	this.$router.push('/signin')
		// } else {
		// 	this.$router.push('/')
		// }
	},
	render: create => create(App)
})