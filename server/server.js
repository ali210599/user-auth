const express = require('express')
const cors = require('cors')
const config = require('./config')
process.env.DEBUG = 'app:*'
const debug = require('debug')

const db = require('./models/')

const logger = debug('app:server')
const PORT = process.env.PORT || config.PORT

const signinRouter = require('./controllers/signin')
const signupRouter = require('./controllers/signup')
const cabinetRouter = require('./controllers/cabinet')

const app = express()


app.use(express.json())
app.use(cors())

/// Including routers
app.use(signinRouter)
app.use(signupRouter)
app.use('/cabinet', cabinetRouter)


db.connect(config.db)
.then(() => {
	app.listen(PORT, () => logger(`SERVER STARTED ON PORT ${PORT}`))
})