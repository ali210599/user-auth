const express = require('express')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const logger = require('debug')('app:signin')
const signin = require('../models/signin')
const config = require('../config')
const router = express.Router()

router.post('/signin', async (req, res) => {
	logger(`Body`, req.body)
	const {email, password} = req.body
	try {
		var user = await signin.getUserByEmail(req.body.email)
		if (!user) throw new Error('User not found')
		if (!(await bcrypt.compare(password, user.password))) throw new Error('Password not matching')
		var token = jwt.sign({
			uid: user.id,
			email: user.email
		}, config.jwt.secret)
	} catch(ex) {
		logger('ERROR', ex.message)
		return res.status(400).json({
			error: 'Invalid email or password'
		})
	}

	res.json({token})
})


module.exports = router