const express = require('express')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const logger = require('debug')('app:signin')
const cabinet = require('../models/cabinet')
const config = require('../config')
const router = express.Router()


router.post('/userData', (req, res) => {
  try {
    const dToken = jwt.verify(config.jwt.secret, req.headers['access-token'])
    if (!dToken) throw new Error('Token not valid or not exists')
  } catch(ex) {
    return res.status(401).json({
      error: 'Token not provided'
    })
  }

  res.json({
    email: userData.email
  })
})