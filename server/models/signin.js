const db = require('./')

exports.getUserByEmail = async email => {
	return (await db.get().query('SELECT * FROM users WHERE email = ?', [email]))[0]
}